@extends('layouts.master')
@section('title','')
<<<<<<< HEAD

=======
    
>>>>>>> Initial commit

@section('content')
<div class="section-body">
</div>

<div class="buttons">
    <div class="card">
        <div class="card-header">
<<<<<<< HEAD
          <h4>Data Barang</h4>
        </div>
        <div class="card-body">
          <div class="alert alert-info">
            <b>Note!</b> Pastikan Isi data dengan Baik dan Benar!
          </div>
         <form action="{{route('cr.update',$data_obat->id)}}"  method="POST" enctype="multipart/form-data">
          @csrf
          @method ('patch')
          <div class="form-group">
            <label for="kode_obat" @error('kode_obat')
                class="text-danger"
            @enderror> Kode Obat @error('kode_obat')
               {{$message}}
              @enderror</label>
            <input name="kode_obat"  type="text"
            @if (old('kode_obat'))
              value="{{old('kode_obat')}}"
            @else
              value="{{$data_obat->kode_obat}}"
            @endif
            class="form-control" ></input>
          </div>

          <div class="form-group">
            <label for="nama_obat" @error('nama_obat')
                class="text-danger"
            @enderror> Nama Obat @error('nama_obat')
               {{$message}}
              @enderror</label>
            <input name="nama_obat"  type="text"
            @if (old('nama_obat'))
              value="{{old('nama_obat')}}"
            @else
              value="{{$data_obat->nama_obat}}"
            @endif
            class="form-control" ></input>
          </div>

          <div class="form-group">
            <label for="stok_obat" @error('stok_obat')
                class="text-danger"
            @enderror> Stok Obat @error('stok_obat')
               {{$message}}
              @enderror</label>
            <input name="stok_obat"  type="text"
            @if (old('stok_obat'))
              value="{{old('stok_obat')}}"
            @else
              value="{{$data_obat->stok_obat}}"
            @endif
            class="form-control" ></input>
          </div>

          <div class="form-group">
            <label for="bentuk_obat" @error('bentuk_obat')
            class="text-danger"
            @enderror> Bentuk Obat @error('bentuk_obat')
            {{$message}}
            @enderror</label>
                <select class="form-control" name="bentuk_obat" value="bentuk_obat">
                    <option value="{{$data_obat->bentuk_obat}}">{{$data_obat->bentuk_obat}}</option>
                    @foreach ($bentuk_obat as $item)
                        @if ( $item !== $data_obat->bentuk_obat )
                            <option value="{{$item}}">{{$item}}</option>
                        @endif
                    @endforeach
                </select>
        </div>

          <div class="form-group">
            <label for="konsumen_obat" @error('konsumen_obat')
                class="text-danger"
            @enderror> Konsumen Obat @error('konsumen_obat')
               {{$message}}
              @enderror</label>
            <input name="konsumen_obat"  type="text"
            @if (old('konsumen_obat'))
              value="{{old('konsumen_obat')}}"
            @else
              value="{{$data_obat->konsumen_obat}}"
            @endif
            class="form-control" ></input>
          </div>

          <div class="form-group">
            <label for="manfaat_obat" @error('manfaat_obat')
                class="text-danger"
            @enderror> Manfaat Obat @error('manfaat_obat')
               {{$message}}
              @enderror</label>
            <input name="manfaat_obat"  type="text"
            @if (old('manfaat_obat'))
              value="{{old('manfaat_obat')}}"
            @else
              value="{{$data_obat->manfaat_obat}}"
            @endif
            class="form-control" ></input>
          </div>

          <div class="form-group">
            <label for="harga_obat" @error('harga_obat')
                class="text-danger"
            @enderror> Harga Obat @error('harga_obat')
               {{$message}}
              @enderror</label>
            <input name="harga_obat"  type="text"
            @if (old('harga_obat'))
              value="{{old('harga_obat')}}"
            @else
              value="{{$data_obat->harga_obat}}"
            @endif
            class="form-control" ></input>
          </div>
=======
          <h4>HTML5 Form Basic</h4>
        </div>
        <div class="card-body">
          <div class="alert alert-info">
            <b>Note!</b> Not all browsers support HTML5 type input.
          </div>
          <div class="form-group">
            <label>Text</label>
            <input type="text" class="form-control">
          </div>
          <div class="form-group">
            <label>Select</label>
            <select class="form-control">
              <option>Option 1</option>
              <option>Option 2</option>
              <option>Option 3</option>
            </select>
          </div>
         
          <div class="form-group">
            <label>Textarea</label>
            <textarea class="form-control"></textarea>
          </div>
          <div class="form-group">
            <label class="d-block">Checkbox</label>
            <div class="form-check">
              <input class="form-check-input" type="checkbox" id="defaultCheck1">
              <label class="form-check-label" for="defaultCheck1">
                Checkbox 1
              </label>
            </div>
            <div class="form-check">
              <input class="form-check-input" type="checkbox" id="defaultCheck3">
              <label class="form-check-label" for="defaultCheck3">
                Checkbox 2
              </label>
            </div>
          </div>
          
          <div class="form-group">
            <label>Date</label>
            <input type="date" class="form-control">
          </div>
         
          <div class="form-group">
            <label>Email</label>
            <input type="email" class="form-control">
          </div>
          <div class="form-group">
            <label>File</label>
            <input type="file" class="form-control">
          </div>
          6
         
          <div class="form-group">
            <label class="d-block">Radio</label>
            <div class="form-check">
              <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" checked>
              <label class="form-check-label" for="exampleRadios1">
                Radio 1
              </label>
            </div>
            <div class="form-check">
              <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" checked>
              <label class="form-check-label" for="exampleRadios2">
                Radio 2
              </label>
            </div>
          </div>
          
      
         
>>>>>>> Initial commit
        </div>
        <div class="card-footer text-right">
          <button class="btn btn-primary mr-1" type="submit">Submit</button>
          <button class="btn btn-secondary" type="reset">Reset</button>
        </div>
<<<<<<< HEAD
      </form>
      </div>
    </div>

=======
      </div>
     
     
     
    
      
    </div>
    
>>>>>>> Initial commit

  </div>
@endsection
@push('page-scripts')
<<<<<<< HEAD

@endpush
=======
       
@endpush
>>>>>>> Initial commit
